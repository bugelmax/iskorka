$(document).ready(function() {
    $(".burger").click(function() {
        $(".burger").toggleClass("is-active");
        $("header").toggleClass("is-active");

    });
    $('.quotes_slider').slick({
        arrows: true,
        dots: false,
        infinite: false,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false
                }
            }
            ]
        }
    );
    $('.reviews_slider').slick({
        arrows: true,
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 10000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false
                }
            }
        ]
        }
    );
    $('.game_images_slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: true,
        dots: false
    });

    $('header .send').click(function(){
        $('.modal_box').addClass('mail_box').addClass('active');
    });
    $('.owerlay, .closeModal, .submit').click(function(){
        $('.modal_box').removeClass().addClass('modal_box');
    });
    $(".wpcf7-submit").click(function(event) {
        $( document ).ajaxComplete(function() {
            $('.modal_box').addClass('sentMessage').addClass('active');
            $('.modal_box').removeClass('mail_box').addClass('active');
        });
    });
});